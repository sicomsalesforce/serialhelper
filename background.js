
/* 
* Chrome Event Listener 
* Responds to/relays events to/from popup.js and contentScript.js
*/
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    //parse received input from popup.js
    if( request.message === "clicked_ok_action" ) {
        parseSerialNumbers(request.text);
    //send message to contentScript to autopopulate serial numbers using quantity and lot information
    } else if( request.message === "clicked_auto_action"){
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {"message": "auto_populate"});
        });
    //send message to popup.js to change styling based on page and value set from contentScript
    } else if( request.message === "check_page" ) {      
        chrome.storage.local.get('urlId', function(result) {
            if(result.urlId!=null){
                chrome.runtime.sendMessage({"message": "change_popup", "urlId" : result.urlId});
            }
        });
    //Listens to Content Script, query page mapping and stores data in local storage for content script to use    
    } else if (request.message === "getPageName" ){
        var baseSF = '.my.salesforce.com';
        var queryString = request.queryString;
        //console.log(queryString);
            chrome.storage.local.get('sid', function(result) {
                if(result.sid!=null){
                    var sid = result.sid;
                    console.log(sid);
                 }
            chrome.storage.local.get('sfInstance', function(result) {
                if(result.sfInstance!=null){
                var sfInstance = result.sfInstance;
                //console.log(sfInstance);
                 }
            jQuery.ajax({
                type: "GET",
                url: "https://" + sfInstance + baseSF + "/services/data/v24.0/query?q=" + queryString,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', "OAuth " + sid);
                    xhr.setRequestHeader('Accept', "application/json");
                },
                success: function (data) {
                    console.log(data);
                    var datarecord = data.records[0];
                    console.log(datarecord);
                    chrome.storage.local.set({record:datarecord});
                },
                error : function(){
                 alert('Request failed, could not retrieve page mappings from salesforce. Try refreshing the page and trying again. If the problem persists, contact your Saleforce Administrator');
                 }
            });

        });
    });

    }

});


/* 
* Handles parsing of serial numbers using newline character
* Stores parsed array in chrome local storage
* Sends message to contentScript to use user input to populate serial numbers
*/
 function parseSerialNumbers(serialList){
    var serialArray = serialList.trim().split("\n");
    chrome.storage.local.set({snArray: serialArray});
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {"message": "manual_populate"});
    });
 }

