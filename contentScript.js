var SalesOrder = 'PackSO';
var ProductionWorkOrder = 'CompletePWO';
var TransferShip = 'ShipTransferOrderLines';
var ItemAdjust = 'ItemInventoryAdjustment'; 
var LotToLot = 'Lot_To_Lot_Item_Movement';
var ItemMove = 'ItemMovement';
var baseSF = '.my.salesforce.com';
var url = window.location.toString();
var serialNumbers = [];
var sfInstance;
var queryString;

/*
* Check if URL was appended by salesforce global search and reload window without suffix
*/
if(url.includes('srPos')){
    window.open(url.split('?')[0], '_self');
}

/*
* Gets sessionId from page and stores it in local storage once
*/
chrome.storage.local.get('sid', function(result) {
    if(url.includes(baseSF)){
        sessionId = document.cookie.match(/(^|;\s*)sid=(.+?);/)[2];
        console.log(sessionId);
        if(result.sid==undefined || result.sid!=sessionId){
            chrome.storage.local.set({sid: sessionId});
            console.log(sid);
        }
    }
});

/*
* Gets sf instance from url and stores it in local storage once
*/
chrome.storage.local.get('sfInstance', function(result) {
    if(url.includes(baseSF) && (sfInstance==undefined || result.sfInstance==undefined)){
        var instance = url.split('//').pop().split('/')[0].replace(baseSF, '');
        chrome.storage.local.set({sfInstance: instance});
        sfInstance = instance;
    } else {
        sfInstance = result.sfInstance;
    }
});


/* 
* Chrome Event Listener 
* Responds to events from background.js
* Determines what the current page is and sets variables
*/
chrome.runtime.onMessage.addListener(
    function(request, sender) {
        var message = request.message;

        if(url.includes(SalesOrder)){
            getPageMapping(SalesOrder, null, message);
        } else if(url.includes(ProductionWorkOrder)){
            getPageMapping(ProductionWorkOrder, null, message);
        } else if (url.includes(TransferShip)) {
            getPageMapping(TransferShip, null, message);
        } else if(url.includes(ItemAdjust)) {
            var tab = $('a[tabindex="0"]').attr("id");
            //add inventory
            if(tab === 'tab-default-1__item'){
                getPageMapping(ItemAdjust, 1, message);
            //subtract inventory
            } else if (tab === 'tab-default-2__item'){
                getPageMapping(ItemAdjust, 2, message);
            }
        } else if (url.includes(LotToLot)){
             getPageMapping(LotToLot, null, message);
        } else if (url.includes(ItemMove)){
            getPageMapping(ItemMove, null, message);
        } else {
            alert('Error: Could not find correct serial number input fields. Please make sure you are on a page that allows for use of Serailized Inventory Helper. If the problem persists please check with your System Administrator.');
        }
    }
);

/*
* Queries page element identifiers from Salesforce organization
* Passes identifiers to getPageElements to retrieve actual elements based on identifiers
*/
function getPageMapping(pageName, tab, message){
    serialNumbers = [];

    chrome.storage.local.get('sid', function(result) {
        if(result.sid!=undefined && result.sid!=null){
            if(tab!=null){
                queryString = "SELECT+Elements_Tag__c,Individual_Lot_Tag__c,Item_Tag__c,Lot_Wrapper_Tag__c+FROM+SIH_Page_Mapping__mdt+WHERE+Page_Name__c+=+'" + pageName + "'+AND+Tab__c+=+'" + tab +"'+LIMIT+1";
            } else{
                queryString = "SELECT+Elements_Tag__c,Individual_Lot_Tag__c,Item_Tag__c,Lot_Wrapper_Tag__c+FROM+SIH_Page_Mapping__mdt+WHERE+Page_Name__c+=+'" + pageName + "'+LIMIT+1";
            }
            console.log(queryString);
            chrome.runtime.sendMessage({"message": "getPageName", 'queryString': queryString});

            chrome.storage.local.get('record', function(result){
              var record = result.record;
              console.log(record);
                    if (record != undefined){ 
                        console.log(record);
                        var lots = $(record.Lot_Wrapper_Tag__c);
                        if(message === "auto_populate" ) {
                            getPageElements(lots, record.Elements_Tag__c, record.Item_Tag__c, record.Individual_Lot_Tag__c, result.sid, pageName, tab);
                        } else if(message === "manual_populate"){
                            getPageElements(lots, record.Elements_Tag__c, record.Item_Tag__c, record.Individual_Lot_Tag__c, null, pageName, tab);
                        } 
                         
                    } else {
                        alert('Error: Could not find correct serial number input fields. Please make sure you are on a page that allows for use of Serailized Inventory Helper. If the problem persists please check with your System Administrator.');
                    }
               });
/*
* Ajax call moved to Background script due to Chrome API Changes to Cross-Origin Requests in Chrome Extension Content Scripts
* 
*/

            // jQuery.ajax({
            //     type: "GET",
            //     url: "https://" + sfInstance + baseSF + "/services/data/v24.0/query?q=" + queryString,
            //     beforeSend: function (xhr) {
            //         xhr.setRequestHeader('Authorization', "OAuth " + result.sid);
            //         xhr.setRequestHeader('Accept', "application/json");
            //     },
            //     success: function (data) {
            //         consle.log(data);
            //         if(data!=undefined){
            //             var record = data.records[0];
            //             var lots = $(record.Lot_Wrapper_Tag__c);
            //             if(message === "auto_populate" ) {
            //                 getPageElements(lots, record.Elements_Tag__c, record.Item_Tag__c, record.Individual_Lot_Tag__c, result.sid, pageName, tab);
            //             } else if(message === "manual_populate"){
            //                 getPageElements(lots, record.Elements_Tag__c, record.Item_Tag__c, record.Individual_Lot_Tag__c, null, pageName, tab);
            //             }   
            //         } else {
            //             alert('Error: Could not find correct serial number input fields. Please make sure you are on a page that allows for use of Serailized Inventory Helper. If the problem persists please check with your System Administrator.');
            //         }
            //     },
            //     error : function(){
            //         alert('Request failed, could not retrieve page mappings from salesforce. Try refreshing the page and trying again. If the problem persists, contact your Saleforce Administrator');
            //     }
            // });
        } else {
            alert('Error: Invalid sessionId. Please exit from the page and try again. ');
            return false;
        }
    });
}


/*
* Gets actual page elements from identifiers on visualforce page and passes them processDataCallout to query salesforce organization 
*/
function getPageElements(lotIds, elementsIdentifier, itemIdentifier, formIdentifier, sessionId, pageName, tab){

    var lotId, itemId, elements;
    if(sessionId!=null){ //if auto populate
        if(lotIds!=undefined && lotIds!=null){
            jQuery.each(lotIds, function( i, val ) {
                lotId = $(this).children('option:selected').val();
                console.log('lotId: ', lotId);
                    if(formIdentifier==null){
                        itemId = $('*[id*="' + i + itemIdentifier + '"]').children('a').attr('href').split('/').pop();
                        elements = $('input[name*="' + i + ':' + elementsIdentifier + '"]');
                    } else {
                        itemId = $('form[id*="' + formIdentifier + '"]').attr('action').split('=').pop();
                        elements =  $('input[name*="' + elementsIdentifier + '"]');
                    }
                processDataCallout(lotId, elements.length, itemId, elements, sessionId, pageName, tab);
            });
        } else {
             alert('Auto Populate is not available for Sales Orders. You must use the manual entry tab.');
             return false;
        }
    } else { //manual populate
        elements =  $('input[name*="' + elementsIdentifier + '"]');
        chrome.storage.local.get('snArray', function(result) {
            jQuery.each(result.snArray, function( i, val ) {
                 serialNumbers[i] = result.snArray[i];
            });
            populatePageElements(serialNumbers, elements.length, elements);
        });
        //remove array from local storage
        chrome.storage.local.remove('snArray');
    }
}

/*
* Queries correct serial numbers from Salesforce organization
* Parses returned serial numbers into an array and passes array to populatePageElements to fill in data 
*/
function processDataCallout(lotId, quantity, itemId, elements, sessionId, pageName, tab){
    if(pageName==TransferShip || (pageName==ItemAdjust && tab=='2')){
        queryString = "SELECT+PBSI__Serial_Number__c+FROM+PBSI__Serial_Number_Tracker__c+WHERE+PBSI__Lot__c+IN+(+SELECT+PBSI__Lot__c+FROM+PBSI__PBSI_Inventory__c+WHERE+Id+=+'" + lotId + "'+)+AND+Serial_Helper_Eligible__c+=+" +true+ "+AND+PBSI__Item__c+=+'" + itemId + "'LIMIT+" + quantity;
    } else {
        queryString = "SELECT+PBSI__Serial_Number__c+FROM+PBSI__Serial_Number_Tracker__c+WHERE+PBSI__Lot__c+=+'" + lotId+ "'+AND+Serial_Helper_Eligible__c+=+" +true+ "+AND+PBSI__Item__c+=+'" + itemId + "'LIMIT+" + quantity;
    }
    jQuery.ajax({
        type: "GET",
        url: "https://" + sfInstance + baseSF + "/services/data/v24.0/query/?q=" + queryString,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', "OAuth " + sessionId);
            xhr.setRequestHeader('Accept', "application/json");
        },
        success: function (data) {
            if(data.totalSize==0 && quantity!=0){
                alert('For one of your items, there were no serial numbers available that that match the Quantity and Lot you selected, you must use the manual entry tab.');
            } else {
                if(data.totalSize<elements.length){
                  alert('For one of your items, there were less serial numbers available that match the Quantity and Lot you selected, you must use the manual entry tab to fill in the rest of the form or change your selections and try again.');  
                }
                jQuery.each(data.records, function( i, val ) {
                    serialNumbers[i] = data.records[i].PBSI__Serial_Number__c;
                });
                populatePageElements(serialNumbers, elements.length, elements);
            }
        }, 
        error : function(){
            alert('Request failed, could not retrieve serial numbers from salesforce. Try refreshing the page and trying again. If the problem persists, contact your Saleforce Administrator');
        }
    });
}

/*
* Populates serial number input fields with returned or manually entered serial numbers
*/
function populatePageElements(serialNumbers, quantity, elements){
    if(serialNumbers!=undefined && elements!=undefined && quantity!=undefined){
        var serialCounter = 0;
        if(quantity>0){
            jQuery.each(elements, function( i, val ) {
                if(!$(this).val() && serialNumbers[serialCounter]!=undefined){
                    elements[i].value = serialNumbers[serialCounter];
                    serialCounter++;
                }
            });
        }   
    } else {
        alert('Unknown error, could not populate. Please try again.');
        return false;
    }
}

