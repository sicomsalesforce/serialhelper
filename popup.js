var itemId;
var sfInstance;

/* 
* Injects new instance of onpopup.js every time the popup/browser action is clicked
* Adds style to popup.html
*/
chrome.tabs.executeScript({ file : 'onPopup.js'}, function() {
    chrome.runtime.sendMessage({"message": "check_page"});
    $("#picklist").removeClass("show").addClass("hide");
    $("#serialInput").removeClass("hide").addClass("show");
});

/* 
* Chrome Event Listener
* Responds to events from background.js
*/
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if( request.message === "change_popup" ) {
        itemId = request.urlId;
        getSFInstance();
       $("#picklist").removeClass("hide").addClass("show");
       $("#serialInput").removeClass("show").addClass("hide");
    } 
  }
);

/*
* Gets sf instance from url and appends Ascent namespace for url redirects
*/
function getSFInstance(){
    chrome.storage.local.get('sfInstance', function(result) {
        
        if(result.sfInstance!=undefined){
            var strings = result.sfInstance.split('.');
            console.log('sfInstnace: ', strings);
            if(strings.length>1){
                sfInstance = strings[0] + '--pbsi.' + strings[1] + '.visual.force.com/apex/';
            } else {
                sfInstance = strings[0] + '--pbsi.' + 'na30.visual.force.com/apex/';
            }
        }
    });
}

/*
* Responds to click of Manual Tab shows correct content
*/
manualTab.onclick = function(element){
    $("#manual").removeClass("hide").addClass("show");
    $("#auto").removeClass("show").addClass("hide");
},

/*
* Responds to click of Auto Tab shows correct content
*/
autoTab.onclick = function(element){
    $("#manual").removeClass("show").addClass("hide");
    $("#auto").removeClass("hide").addClass("show");
},


/*
* Responds to click of Add to Inventory button and redirects to new window using @parameter itemId
*/
adjButton.onclick = function(element){
    window.open('https://'+ sfInstance + 'ItemInventoryAdjustments?id=' + itemId,'_blank');
},

/*
* Responds to click of Lot to Lot Movement button and redirects to new window using @parameter itemId
*/
lotButton.onclick = function(element){
    window.open('https://'+ sfInstance + 'Lot_To_Lot_Item_Movement?id=' + itemId, '_blank');
},

/*
* Responds to click of Item Movement button and redirects to new window using @parameter itemId
*/
itemButton.onclick = function(element){
    window.open('https://'+ sfInstance + 'ItemMovement?id=' + itemId, '_blank');
}

/*
* Responds to click of OK button and sends user input mesesage to background.js for processing
*/
okButton.onclick = function(element){
    chrome.runtime.sendMessage({"message": "clicked_ok_action", "text" : document.getElementById('serialNumberInput').value});
    window.close();
},

/*
* Responds to click of Autopopulate button and sends mesesage to background.js for processing
*/
autoButton.onclick = function(element){
    chrome.runtime.sendMessage({"message": "clicked_auto_action"});
    window.close();
},

/*
* Responds to click of Cancel button and closes popup
*/
cancelButton.onclick = function(element){
    window.close();
}



