var ItemAdjust = 'ItemInventoryAdjustment'; 
var LotToLot = 'Lot_To_Lot_Item_Movement';
var ItemMove = 'ItemMovement';
var url = window.location.toString();

/*
* Runs everytime popup is clicked
* Checks for specific iframes on record page and stores recordId from url in chrome local storage
*/
var iframes = $('html').contents().find('iframe');

if(iframes.length>0){
    jQuery.each(iframes, function( i, val ) {
        //store recordID from url in local storage for popup.js --> we are on the right page
        if(iframes[i].title==ItemAdjust || iframes[i].title==LotToLot || iframes[i].title==ItemMove){

            itemId = url.split('/').pop();
            chrome.storage.local.set({urlId: itemId});
            return false;
        }
        chrome.storage.local.set({urlId: null});
    });
} else {
    //set recordId to null because we did not find the correct iframes --> we are not on the right page
    chrome.storage.local.set({urlId: null});
}







